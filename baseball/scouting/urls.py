from django.urls import path
from scouting import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path('scouting/report', views.ReportScout.as_view()),
    path('scouting/report/manual', views.ReportScoutManual.as_view()),
    path('scouting/report/year', views.ReportYear.as_view()),
    path('scouting/report/all', views.ReportAll.as_view()),
    path('scouting/report/all/manual', views.ReportAllManual.as_view()),
    path('scouting/report/pitcher/<int:pk>/', views.ReportGetPitcher.as_view()),
    path('scouting/report/position/<int:pk>/', views.ReportGetPosition.as_view()),
    path('scouting/report/set/pitcher/<int:pk>/', views.ReportSetPitcher.as_view()),
    path('scouting/report/set/position/<int:pk>/', views.ReportSetPosition.as_view()),

]

urlpatterns = format_suffix_patterns(urlpatterns)