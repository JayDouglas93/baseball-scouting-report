from django.shortcuts import render

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics

from django.http import Http404, HttpResponse, JsonResponse
from django.core.exceptions import SuspiciousOperation

from scouting.models import Report, PositionReport, PitcherReport
from scouting.serializers import ReportSerializer, PitcherReportSerializer, PositionReportSerializer
import json

# Create your views here.

# View for all reports
class ReportAll(generics.ListAPIView):

    queryset = Report.objects.all()
    serializer_class = ReportSerializer

# View all reports of specific year
class ReportYear(generics.ListAPIView):

    queryset = Report.objects.all()
    serializer_class = ReportSerializer

class ReportScout(generics.ListCreateAPIView):

    queryset = Report.objects.all()
    serializer_class = ReportSerializer

class ReportSetPitcher(generics.ListCreateAPIView):
    queryset = PitcherReport.objects.all()
    serializer_class = PitcherReportSerializer

class ReportSetPosition(generics.ListCreateAPIView):
    queryset = PositionReport.objects.all()
    serializer_class = PositionReportSerializer

class ReportGetPitcher(APIView):

    def get_object(self, pk):
        try:
            return PitcherReport.objects.get(pk=pk)
        except PitcherReport.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        scout = self.get_object(pk)
        serializer = PitcherReportSerializer(scout)
        return Response(serializer.data)

class ReportGetPosition(APIView):

    def get_object(self, pk):
        try:
            return PositionReport.objects.get(pk=pk)
        except PositionReport.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        scout = self.get_object(pk)
        serializer = PositionReportSerializer(scout)
        return Response(serializer.data)


class ReportAllManual(APIView):

    def get(self, request, format=None):
        reports = Report.objects.all()
        serializer = ReportSerializer(reports, many=True)
        return Response(serializer.data)

class ReportScoutManual(APIView):

    def post(self, request, format=None):

        print(request.data)
        posOn = request.data.get("isposition")
        pitOn = request.data.get("ispitcher")
        newdata = request.data.copy()

        if posOn == "on":
            positionSer = PositionReportSerializer(data=request.data)

            if positionSer.is_valid():
                pos = positionSer.save()
                newdata.__setitem__("positions", pos.pk)

        if pitOn == "on":
            pitcherSer = PitcherReportSerializer(data=request.data)

            if pitcherSer.is_valid():
                pit = pitcherSer.save()
                newdata.__setitem__("pitchers", pit.pk)

        reportSer = ReportSerializer(data=newdata)

        if reportSer.is_valid():
            reportSer.save()
            return Response(reportSer.data, status=status.HTTP_201_CREATED)

        return Response(reportSer.errors, status=status.HTTP_400_BAD_REQUEST)

