# Generated by Django 2.2.4 on 2019-09-02 16:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scouting', '0002_auto_20190830_0045'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pitcherreport',
            name='date',
        ),
        migrations.AddField(
            model_name='report',
            name='date',
            field=models.DateField(db_column='Date', blank=True, null=True),
            preserve_default=False,
        ),
    ]
