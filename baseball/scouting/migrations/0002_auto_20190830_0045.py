# Generated by Django 2.2.4 on 2019-08-30 04:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scouting', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pitcherreport',
            name='control_vel',
        ),
        migrations.AddField(
            model_name='pitcherreport',
            name='control',
            field=models.SmallIntegerField(blank=True, db_column='Control', null=True),
        ),
    ]
