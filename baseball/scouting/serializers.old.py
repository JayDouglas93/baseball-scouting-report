from rest_framework import serializers
from scouting.models import PitcherReport, PositionReport, Report

class PitcherReportSerializer(serializers.Serializer):

    control = serializers.IntegerField()
    changeup = serializers.IntegerField()
    curveball = serializers.IntegerField()
    cutter = serializers.IntegerField()
    euphus = serializers.IntegerField()
    fastball = serializers.IntegerField()
    forkball = serializers.IntegerField()
    knuckleball = serializers.IntegerField()
    screwball = serializers.IntegerField()
    sinker = serializers.IntegerField()
    slider = serializers.IntegerField()
    splitter = serializers.IntegerField()
    changeup_vel = serializers.IntegerField()
    curveball_vel = serializers.IntegerField()
    cutter_vel = serializers.IntegerField()
    euphus_vel = serializers.IntegerField()
    fastball_vel = serializers.IntegerField()
    forkball_vel = serializers.IntegerField()
    knuckleball_vel = serializers.IntegerField()
    screwball_vel = serializers.IntegerField()
    sinker_vel = serializers.IntegerField()
    slider_vel = serializers.IntegerField()
    splitter_vel = serializers.IntegerField()

    def create(self, validated_data):

        return PitcherReport.objects.create(**validated_data)

    def update(self, instance, validated_data):

        instance.control = validated_data.get('control', instance.control)
        instance.changeup = validated_data.get('changeup', instance.changeup)
        instance.curveball = validated_data.get('curveball', instance.curveball)
        instance.cutter = validated_data.get('cutter', instance.cutter)
        instance.euphus = validated_data.get('euphus', instance.euphus)
        instance.fastball = validated_data.get('fastball', instance.fastball)
        instance.forkball = validated_data.get('forkball', instance.forkball)
        instance.knuckleball = validated_data.get('knuckleball', instance.knuckleball)
        instance.screwball = validated_data.get('screwball', instance.screwball)
        instance.sinker = validated_data.get('sinker', instance.sinker)
        instance.slider = validated_data.get('slider', instance.slider)
        instance.splitter = validated_data.get('splitter', instance.splitter)
        instance.changeup_vel = validated_data.get('changeup_vel', instance.changeup_vel)
        instance.curveball_vel = validated_data.get('curveball_vel', instance.curveball_vel)
        instance.cutter_vel = validated_data.get('cutter_vel', instance.cutter_vel)
        instance.euphus_vel = validated_data.get('euphus_vel', instance.euphus_vel)
        instance.fastball_vel = validated_data.get('fastball_vel', instance.fastball_vel)
        instance.forkball_vel = validated_data.get('forkball_vel', instance.forkball_vel)
        instance.knuckleball_vel = validated_data.get('knuckleball_vel', instance.knuckleball_vel)
        instance.screwball_vel = validated_data.get('screwball_vel', instance.screwball_vel)
        instance.sinker_vel = validated_data.get('sinker_vel', instance.sinker_vel)
        instance.slider_vel = validated_data.get('slider_vel', instance.slider_vel)
        instance.splitter_vel = validated_data.get('splitter_vel', instance.splitter_vel)

        instance.save()
        return instance

class PositionReportSerializer(serializers.Serializer):

    hitting = serializers.IntegerField()
    power = serializers.IntegerField()
    running = serializers.IntegerField()
    fielding = serializers.IntegerField()
    throwing = serializers.IntegerField()

    def create(self, validated_data):

        return PositionReport.objects.create(**validated_data)

    def update(self, instance, validated_data):

        instance.hitting = validated_data.get('hitting', instance.hitting)
        instance.power = validated_data.get('power', instance.power)
        instance.running = validated_data.get('running', instance.running)
        instance.fielding = validated_data.get('fielding', instance.fielding)
        instance.throwing = validated_data.get('throwing', instance.throwing)

        instance.save()
        return instance


class ReportSerializer(serializers.Serializer):

    lastname = serializers.CharField()
    firstname = serializers.CharField()
    position = serializers.CharField()
    team = serializers.CharField()
    description = serializers.TextField()
    futurevalue = serializers.IntegerField()
    positionid = serializers.ForeignKey(PositionReport, serializers.DO_NOTHING, db_column='Position_ID', blank=True, null=True)
    pitcherid = serializers.ForeignKey(PitcherReport, serializers.DO_NOTHING, db_column='Pitcher_ID', blank=True, null=True)

    def create(self, validated_data):

        return Report.objects.create(**validated_data)

    def update(self, instance, validated_data):

        instance.lastname = validated_data.get('lastname', instance.lastname)
        instance.firstname = validated_data.get('firstname', instance.firstname)
        instance.position = validated_data.get('position', instance.position)
        instance.team = validated_data.get('team', instance.team)
        instance.description = validated_data.get('description', instance.description)
        instance.futurevalue = validated_data.get('futurevalue', instance.futurevalue)
        instance.positionid = validated_data.get('positionid', instance.positionid)
        instance.pitcherid = validated_data.get('pitcherid', instance.pitcherid)

        instance.save()
        return instance
