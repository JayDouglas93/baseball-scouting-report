# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.utils import timezone
import datetime

class Report(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    date = models.DateField(db_column='Date', auto_now_add=True)
    lastname = models.CharField(db_column='LastName', blank=True, null=True, max_length=80)
    firstname = models.CharField(db_column='FirstName', blank=True, null=True, max_length=80)
    position = models.CharField(db_column='Position', blank=True, null=True, max_length=50)
    team = models.CharField(db_column='Team', blank=True, null=True, max_length=100)
    description = models.TextField(db_column='Description', blank=True, null=True)
    futurevalue = models.SmallIntegerField(db_column='FutureValue', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'report'

class PitcherReport(models.Model):

    id = models.AutoField(db_column='ID', primary_key=True)
    control = models.SmallIntegerField(db_column='Control', blank=True, null=True)
    changeup = models.SmallIntegerField(db_column='Changeup', blank=True, null=True)   
    curveball = models.SmallIntegerField(db_column='Curveball', blank=True, null=True)   
    cutter = models.SmallIntegerField(db_column='Cutter', blank=True, null=True)   
    euphus = models.SmallIntegerField(db_column='Euphus', blank=True, null=True)   
    fastball = models.SmallIntegerField(db_column='Fastball', blank=True, null=True)   
    forkball = models.SmallIntegerField(db_column='Forkball', blank=True, null=True)   
    knuckleball = models.SmallIntegerField(db_column='Knuckleball', blank=True, null=True)   
    screwball = models.SmallIntegerField(db_column='Screwball', blank=True, null=True)   
    sinker = models.SmallIntegerField(db_column='Sinker', blank=True, null=True)   
    slider = models.SmallIntegerField(db_column='Slider', blank=True, null=True)   
    splitter = models.SmallIntegerField(db_column='Splitter', blank=True, null=True)   
    changeup_vel = models.SmallIntegerField(db_column='Changeup_Vel', blank=True, null=True)   
    curveball_vel = models.SmallIntegerField(db_column='Curveball_Vel', blank=True, null=True)   
    cutter_vel = models.SmallIntegerField(db_column='Cutter_Vel', blank=True, null=True)   
    euphus_vel = models.SmallIntegerField(db_column='Euphus_Vel', blank=True, null=True)   
    fastball_vel = models.SmallIntegerField(db_column='Fastball_Vel', blank=True, null=True)   
    forkball_vel = models.SmallIntegerField(db_column='Forkball_Vel', blank=True, null=True)   
    knuckleball_vel = models.SmallIntegerField(db_column='Knuckleball_Vel', blank=True, null=True)   
    screwball_vel = models.SmallIntegerField(db_column='Screwball_Vel', blank=True, null=True)   
    sinker_vel = models.SmallIntegerField(db_column='Sinker_Vel', blank=True, null=True)   
    slider_vel = models.SmallIntegerField(db_column='Slider_Vel', blank=True, null=True)   
    splitter_vel = models.SmallIntegerField(db_column='Splitter_Vel', blank=True, null=True)
    report = models.ForeignKey(Report, related_name="pitchers", db_column='Position_ID', blank=True, null=True,
                                 on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'pitcher_report'

class PositionReport(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    hitting = models.SmallIntegerField(db_column='Hitting', blank=True, null=True)   
    power = models.SmallIntegerField(db_column='Power', blank=True, null=True)   
    running = models.SmallIntegerField(db_column='Running', blank=True, null=True)   
    fielding = models.SmallIntegerField(db_column='Fielding', blank=True, null=True)   
    throwing = models.SmallIntegerField(db_column='Throwing', blank=True, null=True)
    report = models.ForeignKey(Report, related_name="positions", db_column='Position_ID', blank=True, null=True,
                                 on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'position_report'

