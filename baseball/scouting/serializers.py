from rest_framework import serializers
from scouting.models import PitcherReport, PositionReport, Report

class PitcherReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = PitcherReport
        fields = ['id', 'control', 'changeup', 'curveball', 'cutter', 'euphus', 'fastball', 'forkball', 'knuckleball', 'screwball', 'sinker', 'slider', 'splitter',
                  'changeup_vel', 'curveball_vel', 'cutter_vel', 'euphus_vel', 'fastball_vel', 'forkball_vel', 'knuckleball_vel', 'screwball_vel', 'sinker_vel', 'slider_vel', 'splitter_vel']

class PositionReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = PositionReport
        fields = ['id', 'hitting', 'power', 'running', 'fielding', 'throwing']

class ReportSerializer(serializers.ModelSerializer):

    positions = serializers.PrimaryKeyRelatedField(many=True, queryset=PositionReport.objects.all())
    pitchers = serializers.PrimaryKeyRelatedField(many=True, queryset=PitcherReport.objects.all())

    # positions = PositionReportSerializer(many=True, required=False, source="positionreport_set")
    # pitchers = PitcherReportSerializer(many=True, required=False, source="pitcherreport_set")
    #
    # def create(self, validated_data):
    #
    #     pitchers_data = validated_data.pop('pitchers')
    #     positions_data = validated_data.pop('positions')
    #
    #     report = Report.objects.create(**validated_data)
    #
    #     #for pitcher_data in pitchers_data:
    #     PitcherReport.objects.create(report=report, **pitchers_data)
    #
    #     #for position_data in positions_data:
    #     PositionReport.objects.create(report=report, **positions_data)
    #
    #     return report

    class Meta:
        model = Report
        fields = ['id', 'lastname', 'firstname', 'position', 'team', 'description', 'futurevalue', 'positions',
                  'pitchers']
