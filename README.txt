Web app uses Django REST framework and bootstrap front-end.
Front-end libraries are included in CDN, no need to install.

Must have:
- Python 3.6
- pip install django
- pip install djangorestframework

To run local server for API:
python manage.py runserver 8080

Web pages:
scouts.html - View all reports
report.html - Add new report

Note: Can open web pages locally without running 'runserver', but API will not work

Uses SQLite3 database (scout.db), which is already local, so no need to start it manually.

Known bugs with Django:
- Localserver will throw connection errors (winerror 10053). Should not effect functionality.
- Django requires up-to-date browser to function, such as Chrome (old versions of Firefox and Edge were not able to access the API)

The web app is missing some form validation and formatting of returned reports, however the main focus was on the overall functionality.
Edge cases such as in-depth bootstrap layouts came second to ensuring proper data entry and retrieval with organized and logical database table relations.